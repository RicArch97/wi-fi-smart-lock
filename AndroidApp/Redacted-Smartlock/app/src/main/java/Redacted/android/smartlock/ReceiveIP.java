package Redacted.android.smartlock;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import timber.log.Timber;

public class ReceiveIP extends AppCompatActivity {
    String text = "Waiting for Smart Hub...";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loadingscreen);

        TextView instruction = findViewById(R.id.loadingText);
        instruction.setText(text);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        BroadcastReceiver receiver = new BroadcastReceiver(this);
        receiver.start();

        Timber.v("Receiver Started");
    }
}
