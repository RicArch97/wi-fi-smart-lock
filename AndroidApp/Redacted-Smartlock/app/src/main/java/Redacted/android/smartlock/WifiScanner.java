package Redacted.android.smartlock;

import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class WifiScanner extends AppCompatActivity {
    //Wifi menu
    WifiManager wifiManager;
    ListView listView;
    Button buttonScan;
    List<ScanResult> results;
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayAdapter adapter;

    //Bluetooth
    OutputStream mmOutputStream;
    BluetoothSocket mmSocket;

    // Wifi wifiscan
    String ssid;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wifiscan);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        mmOutputStream = Helper.getInstance().getOutputStreamer();
        mmSocket = Helper.getInstance().getSocket();

        //components
        buttonScan = findViewById(R.id.scanBtn);
        listView = findViewById(R.id.wifiList);

        buttonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanWifi();
            }
        });

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (!wifiManager.isWifiEnabled()) {
            Toast.makeText(this, "WiFi is disabled ... We need to enable it", Toast.LENGTH_LONG).show();
            wifiManager.setWifiEnabled(true);
        }

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrayList);
        listView.setAdapter(adapter);
        scanWifi();


        //list click listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // build correct string
                ssid = listView.getItemAtPosition(position).toString();

                //display password popup
                AlertDialog.Builder builder = new AlertDialog.Builder(WifiScanner.this);
                builder.setTitle("Password for " + ssid + ":");

                // Set up the input
                final EditText input = new EditText(WifiScanner.this);
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            sendData(ssid);
                            try {
                                Thread.sleep(200);
                            } catch (InterruptedException er) {
                                er.printStackTrace();
                            }
                            sendData(input.getText().toString());

                            // start receiveIP
                            Intent myIntent = new Intent(WifiScanner.this, ReceiveIP.class);
                            startActivityForResult(myIntent, 0);

                            closeBT();
                        } catch (IOException e) {
                            Timber.v("Wifi data could not be send");
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });
    }

    void sendData(String message) throws IOException {
        mmOutputStream.write(message.getBytes());
        Timber.v("Data Sent");
    }

    void closeBT() throws IOException {
        mmOutputStream.close();
        mmSocket.close();
        Timber.v("Bluetooth Closed");
    }

    private void scanWifi() {
        arrayList.clear();
        registerReceiver(wifiReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        wifiManager.startScan();
        Toast.makeText(this, "Scanning WiFi ...", Toast.LENGTH_SHORT).show();
    }

    android.content.BroadcastReceiver wifiReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            results = wifiManager.getScanResults();
            unregisterReceiver(this);

            for (ScanResult scanResult : results) {
                arrayList.add(scanResult.SSID);
                adapter.notifyDataSetChanged();
            }
        }
    };
}
