import os.path
import os
import sys
import socket

# get ip address and broadcast address
ip = os.popen('ip addr show wlan0').read().split("inet ")[1].split("/")[0]

# create socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
s.bind(('', 0))

# broadcast
s.sendto(ip.encode(), ('<broadcast>', 12345))
