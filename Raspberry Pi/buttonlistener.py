from gpiozero import Button, LED
import subprocess
import os
import socket
from signal import pause

button = Button(2, hold_time=5)
led = LED(17)

def activate_bluetooth():
    # setup bluetooth visibilty
    print('making device visible')
    subprocess.Popen(['sudo', 'hciconfig', 'hci0', 'piscan'])
    # subprocess.Popen(['sudo', 'hciconfig', 'hci0', 'name', 'Broker'])

    print('Bluetooth server started')
    # blink led to state that bluetooth has started
    led.blink()

    # start server
    p = subprocess.Popen(['sudo', 'python', '/home/pi/scripts/bluetooth_serial.py'])
    
    # wait for process to finish
    p.wait()

    # turn off led
    led.off()


while True:
    # activate bluetooth
    button.when_held = activate_bluetooth

    pause()
