package Redacted.android.smartlock;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import timber.log.Timber;

public class EnterUserName extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_username);

        if(BuildConfig.DEBUG){
            Timber.plant(new Timber.DebugTree());
        }

        if (readFile(getBaseContext())) {
            Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
            startActivityForResult(myIntent, 0);
        } else {
            // components
            Button nextButton = findViewById(R.id.nextButton);
            final TextView name = findViewById(R.id.inputName);

            nextButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    writeFile("" + name.getText(), view.getContext());
                    Intent myIntent = new Intent(view.getContext(), MainActivity.class);
                    startActivityForResult(myIntent, 0);
                }
            });
        }
    }

    public void writeFile(final String fileContents, Context context) {
        try {
            FileWriter out = new FileWriter(new File(context.getFilesDir(), "username.txt"));
            out.write(fileContents);
            out.close();
        } catch (IOException e) {
            Timber.v("Exception, file write failed %s",e.toString());
        }
    }

    public Boolean readFile(Context context) {
        BufferedReader in;

        try {
            in = new BufferedReader(new FileReader(new File(context.getFilesDir(), "username.txt")));
            if ((in.readLine()) != null) return true;
        } catch (FileNotFoundException e) {
            Timber.v("login activity: file not found %s",e.toString());
        } catch (IOException e) {
            Timber.v("login activity: cannot open file %s", e.toString());
        }

        return false;
    }
}
