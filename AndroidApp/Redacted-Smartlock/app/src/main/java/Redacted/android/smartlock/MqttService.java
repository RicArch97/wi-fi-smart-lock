package Redacted.android.smartlock;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.File;

import timber.log.Timber;

public class MqttService {
    private MqttAndroidClient CLIENT;
    private MqttConnectOptions MQTT_CONNECTION_OPTIONS;
    private Context context;
    private static final String LOCK_TOPIC = "ESP32/locks/#";
    private static final String ALERT_TOPIC = "ESP32/warning";

    public MqttService(MqttAndroidClient CLIENT, MqttConnectOptions options, Context context) {
        this.CLIENT = CLIENT;
        this.MQTT_CONNECTION_OPTIONS = options;
        this.context = context;
    }

    void connect() {
        try {
            final IMqttToken token = CLIENT.connect(MQTT_CONNECTION_OPTIONS);
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    CLIENT.setBufferOpts(getDisconnectedBufferOptions());
                    Timber.v("mqtt: connected, token: %s",asyncActionToken.toString());
                    subscribe(LOCK_TOPIC, (byte) 2);
                    subscribe(ALERT_TOPIC, (byte) 2);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Timber.v("mqtt: not connected %s",asyncActionToken.toString());

                    ShowDialog dialog = new ShowDialog(context);
                    dialog.showDialog();
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    private DisconnectedBufferOptions getDisconnectedBufferOptions() {
        DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
        disconnectedBufferOptions.setBufferEnabled(true);
        disconnectedBufferOptions.setBufferSize(100);
        disconnectedBufferOptions.setPersistBuffer(false);
        disconnectedBufferOptions.setDeleteOldestMessages(false);
        return disconnectedBufferOptions;
    }

    void subscribe(String topic, byte qos) {

        try {
            IMqttToken subToken = CLIENT.subscribe(topic, qos);
            subToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Timber.v("mqtt: subscribed %s",asyncActionToken.toString());
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    Timber.v("mqtt: subscribing error");
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    void publish(String topic, String msg) {

        try {
            IMqttToken token = CLIENT.publish(topic, msg.getBytes(), 2, false);
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Timber.v("mqtt: message published %s",asyncActionToken.toString());
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Timber.v("mqtt: publish error %s", asyncActionToken.toString());

                    ShowDialog dialog = new ShowDialog(context);
                    dialog.showDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void unsubscribe(String topic) {

        try {
            IMqttToken unsubToken = CLIENT.unsubscribe(topic);
            unsubToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {

                    Timber.v("mqtt: unsubscribed");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {


                    Timber.v("mqtt: couldn't unsubscribe");
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    void disconnect() {
        try {
            IMqttToken disconToken = CLIENT.disconnect();
            disconToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Timber.v("mqtt: disconnected");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {


                    Timber.v("mqtt: couldn't disconnect");
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}
