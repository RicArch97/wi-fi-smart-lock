package Redacted.android.smartlock;

import android.content.Context;
import android.content.Intent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import timber.log.Timber;

public class BroadcastReceiver extends Thread {
    private DatagramSocket receiveSocket = null;
    private String data = null;
    private Context context;

    public BroadcastReceiver(Context context) {
        this.context = context;
    }

    public void run() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        while(!(Thread.interrupted())) {
            try {
                if (data != null) {
                    // format IP address
                    String IP = "ssl://" + data + ":8884";

                    // write to file
                    writeFile(IP, context);

                    Timber.v("Data written to file");

                    // start next activity
                    Intent enterName = new Intent(context, EnterUserName.class);
                    context.startActivity(enterName);
                    break;
                }
                else {
                    receive();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private DatagramSocket getReceiveSocket() throws UnknownHostException, SocketException {
        if (receiveSocket == null) {
            receiveSocket = new DatagramSocket(12345, InetAddress.getByName("0.0.0.0"));
            receiveSocket.setBroadcast(true);
        }
        return receiveSocket;
    }

    private void receive() throws IOException {
        byte[] buffer = new byte[1024];
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        getReceiveSocket().receive(packet);

        data = new String(packet.getData()).trim();

        Timber.v("Data: %s", data);
    }

    private void writeFile(final String fileContents, Context context) {
        try {
            FileWriter out = new FileWriter(new File(context.getFilesDir(), "serveripaddress.txt"));
            out.write(fileContents);
            out.close();
        } catch (IOException e) {
            Timber.v("Exception, file write failed %s", e.toString());
        }
    }
}
