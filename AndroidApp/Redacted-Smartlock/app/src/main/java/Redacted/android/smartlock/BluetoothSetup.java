package Redacted.android.smartlock;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import timber.log.Timber;


public class BluetoothSetup extends AppCompatActivity {
    String text = "Waiting for Bluetooth Connection...";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loadingscreen);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        TextView instruction = findViewById(R.id.loadingText);
        instruction.setText(text);

        BluetoothConnection BTcon = new BluetoothConnection(this);
        BTcon.start();
    }
}
