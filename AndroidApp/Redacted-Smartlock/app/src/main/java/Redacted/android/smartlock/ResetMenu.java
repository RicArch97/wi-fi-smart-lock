package Redacted.android.smartlock;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Switch;
import android.widget.TextView;

import java.io.File;

import timber.log.Timber;

public class ResetMenu  {
    private Button resetMenu;
    private Context context;
    private Switch sb;

    public ResetMenu(Button resetMenu, Context context, Switch sb) {
        this.resetMenu = resetMenu;
        this.context = context;
        this.sb = sb;
    }

    public void show() {
        resetMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, resetMenu);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.resetmenu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().toString().equals("Reset Settings")) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Are you sure you want to reset internet connection and user?");

                            // Set up the buttons
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String dir = context.getFilesDir().getAbsolutePath();
                                    File f0 = new File(dir, "username.txt");
                                    boolean d0 = f0.delete();
                                    Timber.v("Delete Check, %s", "File deleted: " + dir + "/myFile " + d0);

                                    String dir2 = context.getFilesDir().getAbsolutePath();
                                    File f1 = new File(dir, "serveripaddress.txt");
                                    boolean d1 = f1.delete();
                                    Timber.v("Delete Check, %s", "File deleted: " + dir2 + "/myFile " + d1);

                                    Intent resetTobase = new Intent(context, Setup.class);
                                    context.startActivity(resetTobase);
                                }
                            });
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                            builder.show();

                            return true;
                        }
                        if (item.getTitle().toString().equals("Security Options")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            final LinearLayout layout = new LinearLayout(context);
                            layout.setOrientation(LinearLayout.HORIZONTAL);

                            TextView text = new TextView(context);
                            text.setTextSize(20);
                            String option = "Popup on opening lock           ";
                            text.setText(option);
                            layout.addView(text);

                            layout.addView(sb);

                            builder.setView(layout);

                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    layout.removeView(sb);
                                    dialog.cancel();
                                }
                            });

                            builder.show();

                            return true;
                        }
                        return false;
                    }
                });

                popup.show();
            }
        });
    }
}
