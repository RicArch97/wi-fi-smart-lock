package Redacted.android.smartlock;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;

import timber.log.Timber;

public class ShowDialog {
    private Context context;

    public ShowDialog(Context context) {
        this.context = context;
    }

    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        final LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.HORIZONTAL);

        final LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(20,20,20,20);

        ImageView noconnection = new ImageView(context);
        noconnection.setImageResource(R.drawable.noconnection);
        layout.addView(noconnection, lp);

        TextView text = new TextView(context);
        text.setTextSize(17);
        String warning = "Could not establish connection to the Smart Hub. Make sure your Smart Hub is powered on, or run the setup again.";
        text.setText(warning);
        layout.addView(text, lp);

        builder.setView(layout);

        builder.setPositiveButton("Run setup", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Are you sure you want to reset internet connection and user?");

                // Set up the buttons
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String dir = context.getFilesDir().getAbsolutePath();
                        File f0 = new File(dir, "username.txt");
                        boolean d0 = f0.delete();
                        Timber.v("Delete Check, %s", "File deleted: " + dir + "/myFile " + d0);

                        String dir2 = context.getFilesDir().getAbsolutePath();
                        File f1 = new File(dir, "serveripaddress.txt");
                        boolean d1 = f1.delete();
                        Timber.v("Delete Check, %s", "File deleted: " + dir2 + "/myFile " + d1);

                        Intent resetTobase = new Intent(context, Setup.class);
                        context.startActivity(resetTobase);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}
