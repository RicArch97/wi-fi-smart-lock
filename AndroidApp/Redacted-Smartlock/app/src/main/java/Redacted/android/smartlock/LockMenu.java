package Redacted.android.smartlock;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import timber.log.Timber;


public class LockMenu {
    private Spinner spinner;
    private ArrayAdapter<String> adapter;
    private List<String> list;
    private String deletedLock;
    private Context context;

    public LockMenu(Context context, Spinner spinner) {
        this.context = context;
        this.spinner = spinner;
    }

    public void init() {
        list = new ArrayList<>();
        adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, list);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        // lock selection
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String item = parent.getItemAtPosition(pos).toString();
                if (item.equals("+Add new lock")) {
                    getLockname();
                }
                if (item.equals("-Delete lock")) {
                    deleteLock(context);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {}
        });

        list.add(0, "-Delete lock");
        list.add(0,"+Add new lock");
        addLocks(list, context);
        adapter.notifyDataSetChanged();
    }

    void addLocks(List<String> list, Context context) {
        String line;
        BufferedReader in;

        try {
            in = new BufferedReader(new FileReader(new File(context.getFilesDir(), "locks.txt")));
            while ((line = in.readLine()) != null) {
                list.add(0, line);
            }
        } catch (FileNotFoundException e) {
            Timber.v("login activity: file not found %s",e.toString());
        } catch (IOException e) {
            Timber.v("login activity: cannot open file %s", e.toString());
        }
    }

    void deleteLock(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose a lock to delete:");
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        final TextView text = new TextView(context);

        // list
        ArrayList<String> arrayList = new ArrayList<>();
        addLocks(arrayList, context);
        ArrayAdapter ap = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, arrayList);
        final ListView lv = new ListView(context);
        lv.setAdapter(ap);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                deletedLock = lv.getItemAtPosition(position).toString();
                StringBuilder builder = new StringBuilder();
                builder.append("This lock will be deleted: ");
                builder.append(deletedLock);
                text.setText(builder.toString());
            }
        });

        layout.addView(lv);
        layout.addView(text);

        builder.setView(layout);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                File tempFile = new File(context.getFilesDir(), "tempfile.txt");
                File inputFile = new File(context.getFilesDir(),"locks.txt");

                try {
                    FileInputStream fIn = new FileInputStream(inputFile);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(fIn));
                    BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

                    String currentLine;

                    while ((currentLine = reader.readLine()) != null) {
                        // trim newline when comparing with lineToRemove
                        String trimmedLine = currentLine.trim();
                        if (trimmedLine.equals(deletedLock)) continue;
                        writer.write(currentLine + System.getProperty("line.separator"));
                    }
                    writer.close();
                    reader.close();
                    boolean successful = tempFile.renameTo(inputFile);

                    if (successful) {
                        dialog.cancel();
                        list.remove(deletedLock);
                        adapter.notifyDataSetChanged();
                        spinner.setSelection(0);
                    }

                } catch (IOException io) {
                    Timber.v(io);
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    void getLockname() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Enter a name for the lock:");

        // Set up the input
        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = input.getText().toString();
                list.add(0, name);
                adapter.notifyDataSetChanged();
                spinner.setSelection(0);

                //write to file
                writeFile(name + "\n", "locks.txt");
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    void writeFile(final String fileContents, String filename) {
        try {
            File input = new File(context.getFilesDir(), filename);
            FileOutputStream fileinput = new FileOutputStream(input, true);
            fileinput.write(fileContents.getBytes());
            fileinput.close();
        } catch (IOException e) {
            Timber.v("Exception, file write failed %s",e.toString());
        }
    }
}
