package Redacted.android.smartlock;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    private MqttAndroidClient CLIENT;
    private MqttService MQTT;
    private TextView mqttData, username;
    private ImageView lock;
    private Button resetMenu;
    private Spinner spinner;
    private Switch sb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //log tree
        if(BuildConfig.DEBUG){
            Timber.plant(new Timber.DebugTree());
        }

        // components
        mqttData = findViewById(R.id.data);
        lock = findViewById(R.id.lockImage);
        username = findViewById(R.id.userNameView);
        spinner = findViewById(R.id.locks);

        // settings
        sb = new Switch(MainActivity.this);
        sb.setTextOff("OFF");
        sb.setTextOn("ON");
        sb.setChecked(false);

        // init
        resetMenu = findViewById(R.id.options);
        ResetMenu reset = new ResetMenu(resetMenu, this, sb);
        reset.show();
        username.setText("User: ");
        username.append(readFile(this, "username.txt"));

        // lock menu
        LockMenu combobox = new LockMenu(this, spinner);
        combobox.init();

        // image view listener
        lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String item = spinner.getSelectedItem().toString();
                final StringBuilder topic = new StringBuilder();
                topic.append("ESP32/");
                topic.append("locks/");
                topic.append(item);

                if (readFile(MainActivity.this, "lockstate.txt").equals("OPEN")) {
                    MQTT.publish(topic.toString(), "CLOSE");
                }
                else if (readFile(MainActivity.this, "lockstate.txt").equals("CLOSE")) {
                    if (sb.isChecked()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Are you sure you want to open lock '" + item + "' ?");

                        // Set up the buttons
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                MQTT.publish(topic.toString(), "OPEN");
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder.show();
                    }
                    else {
                        MQTT.publish(topic.toString(), "OPEN");
                    }
                }
                else {
                    Toast.makeText(MainActivity.this, "Could not get current lockstate", Toast.LENGTH_LONG).show();
                }
            }
        });

        //mqtt wifiscan
        mqttSetup(this, readFile(getBaseContext(), "serveripaddress.txt"));
        mqttCallback();

        writeFile("CLOSE", this, "lockstate.txt");

        // lock init
        if (readFile(this, "lockstate.txt").equals("CLOSE")) {
            lock.setImageResource(R.drawable.ic_action_lockclosed);
            mqttData.setTextColor(Color.parseColor("#ff0000"));
            mqttData.setText("CLOSE");
        }
        else if (readFile(this, "lockstate.txt").equals("OPEN")) {
            lock.setImageResource(R.drawable.ic_action_lockopen);
            mqttData.setTextColor(Color.parseColor("#00ff00"));
            mqttData.setText("OPEN");
        }

    }

    String readFile(Context context, String filename) {
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        BufferedReader in;

        try {
            in = new BufferedReader(new FileReader(new File(context.getFilesDir(), filename)));
            while ((line = in.readLine()) != null) stringBuilder.append(line);
        } catch (FileNotFoundException e) {
            Timber.v("login activity: file not found %s",e.toString());
        } catch (IOException e) {
            Timber.v("login activity: cannot open file %s", e.toString());
        }

        return stringBuilder.toString();
    }

    private void writeFile(final String fileContents, Context context, String filename) {
        try {
            FileWriter out = new FileWriter(new File(context.getFilesDir(), filename));
            out.write(fileContents);
            out.close();
        } catch (IOException e) {
            Timber.v("Exception, file write failed %s", e.toString());
        }
    }

    void mqttSetup(Context context, final String BROKER) {
        MqttConnectOptions MQTT_CONNECTION_OPTIONS;

        CLIENT = new MqttAndroidClient(getBaseContext(), BROKER, MqttClient.generateClientId());
        MQTT_CONNECTION_OPTIONS = new MqttConnectOptions();

        SocketFactory.SocketFactoryOptions socketFactoryOptions = new SocketFactory.SocketFactoryOptions();
        try {
            socketFactoryOptions.withCaInputStream(context.getResources().openRawResource(Redacted.android.smartlock.R.raw.ca));
            MQTT_CONNECTION_OPTIONS.setSocketFactory(new SocketFactory(socketFactoryOptions));
        } catch (IOException | NoSuchAlgorithmException | KeyStoreException | CertificateException | KeyManagementException | UnrecoverableKeyException e) {
            e.printStackTrace();
        }

        MQTT_CONNECTION_OPTIONS.setCleanSession(false);
        MQTT_CONNECTION_OPTIONS.setAutomaticReconnect(true);

        MQTT = new MqttService(CLIENT, MQTT_CONNECTION_OPTIONS, this);
        MQTT.connect();
    }

    void mqttCallback() {
        CLIENT.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {}

            @Override
            public void messageArrived(String topic, MqttMessage message) {
                String data = message.toString();
                updateAttributes(data, topic);
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {}
        });
    }

    void updateAttributes(String data, String topic) {
        if (topic.contains("ESP32/locks/")) {
            if (data.equals("CLOSE")) {
                writeFile("CLOSE", this, "lockstate.txt");
                lock.setImageResource(R.drawable.ic_action_lockclosed);
                mqttData.setTextColor(Color.parseColor("#ff0000"));
                notificationDialog("User " + readFile(this, "username.txt") + " has closed lock " + spinner.getSelectedItem() + ".", "Lock status changed");

                mqttData.setText(data);
            }
            else if (data.equals("OPEN")) {
                writeFile("OPEN", this, "lockstate.txt");
                lock.setImageResource(R.drawable.ic_action_lockopen);
                mqttData.setTextColor(Color.parseColor("#00ff00"));
                notificationDialog("User " + readFile(this, "username.txt") + " has opened lock " + spinner.getSelectedItem() + ".", "Lock status changed");

                mqttData.setText(data);
            }
        }
        if (topic.equals("ESP32/warning")) {
            if (data.equals("ALERT")) {
                notificationDialog("Someone might be trying to break open the lock!", "Lock has detected a significant amount of force");
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                final LinearLayout layout = new LinearLayout(MainActivity.this);
                layout.setOrientation(LinearLayout.HORIZONTAL);

                final LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.setMargins(20, 20, 20, 20);

                ImageView lockbreak = new ImageView(MainActivity.this);
                lockbreak.setImageResource(R.drawable.breaklock);
                layout.addView(lockbreak, lp);

                TextView text = new TextView(MainActivity.this);
                text.setTextSize(17);
                String warning = "Lock has detected a significant amount of force, someone might be trying to break open the lock!";
                text.setText(warning);
                layout.addView(text, lp);

                builder.setView(layout);

                builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        }
    }

    // disable back button in this activity
    @Override
    public void onBackPressed() { }

    private void notificationDialog(String message, String title) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "smartlock_notification";

        NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_DEFAULT);
        // Configure the notification channel.
        notificationChannel.setDescription("Lock messages");
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(Color.RED);
        notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
        notificationChannel.enableVibration(true);
        notificationManager.createNotificationChannel(notificationChannel);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);

        //settings
        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker("Smart Lock")
                //.setPriority(Notification.PRIORITY_MAX)
                .setContentTitle(title)
                .setContentText(message)
                .setContentInfo("Information");
        notificationManager.notify(1, notificationBuilder.build());
    }
}

