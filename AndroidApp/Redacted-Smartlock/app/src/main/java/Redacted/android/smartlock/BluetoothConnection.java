package Redacted.android.smartlock;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import timber.log.Timber;

public class BluetoothConnection extends Thread {
    private Context context;

    // Bluetooth
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothSocket mmSocket;
    private BluetoothDevice mmDevice;
    private OutputStream mmOutputStream;

    public BluetoothConnection(Context context) {
        this.context = context;
    }

    public void run() {
        while(!(Thread.interrupted())) {
            if (findBT()) {

                // create intent
                Intent wifiscan = new Intent(context, WifiScanner.class);
                context.startActivity(wifiscan);
                break;
            }
        }
    }

    private boolean findBT() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Timber.v("No bluetooth adapter available");
        }

        //turn bluetooth on, ask user for permission
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            context.startActivity(enableBluetooth);
        }

        // wait till user confirm selection
        while (!mBluetoothAdapter.isEnabled()) {
        }


        // get list of bluetooth devices and connect to devicename
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equals("raspberrypi")) {
                    mmDevice = device;
                    Timber.v("Bluetooth Device Found");
                    break;
                }
            }
        }

        if (mmDevice == null) {
            //Timber.v("Bluetooth device not found");
            return false;
        }
        else {
            try {
                if (openBT()) {
                    return true;
                }
            } catch (IOException e) {
                //Timber.v("Bluetooth could not be opened");
            }
        }
        return false;
    }

    private boolean openBT() throws IOException {
        // create UUID, standard serialport and connect to socket
        UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
        mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
        mmSocket.connect();
        mmOutputStream = mmSocket.getOutputStream();

        Timber.v("Bluetooth Opened");

        Helper.getInstance().setOutputStreamer(mmOutputStream);
        Helper.getInstance().setSocket(mmSocket);

        return true;
    }
}
