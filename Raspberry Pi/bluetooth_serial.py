import os
import glob
import time
import random
import subprocess

from bluetooth import *

server_sock = BluetoothSocket( RFCOMM )
server_sock.bind(("",PORT_ANY))
server_sock.listen(1)

port = server_sock.getsockname()[1]

uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"

advertise_service( server_sock, "TestServer",
                            service_id = uuid,
                            service_classes = [ uuid, SERIAL_PORT_CLASS ],
                            profiles = [ SERIAL_PORT_PROFILE ])

print ("Waiting for connection on RFCOMM channel %d" % port)

try:
    server_sock.settimeout(20)
    client_sock, client_info = server_sock.accept()
except:
    pass
else:
    print ("Accepted connection from ", client_info)
    with open('/etc/wpa_supplicant/wpa_supplicant.conf', 'a') as file:
        file.write('\n')
        file.write('network={')
        file.close()

    data = []
    
    while True:
        try:
            data.append(client_sock.recv(1024))
            #print ("received [%s]" % data)

            with open('/etc/wpa_supplicant/wpa_supplicant.conf', 'a') as file:
                try:
                    t=data[1]
                    file.write('\n')
                    file.write('\tpsk=')
                    file.write('"')
                    file.write(data[1])
                    file.write('"')
                    file.write('\n')
                    file.write('}')
                    file.close()

                    # restart the dhcpcd server
                    p1 = subprocess.Popen(['sudo', 'systemctl', 'daemon-reload'])
                    p2 = subprocess.Popen(['sudo', 'systemctl', 'restart', 'dhcpcd'])
                    p1.wait()
                    p2.wait()
                    
                    # wait a bit for the internet to be enabled
                    time.sleep(5)

                    # broadcast ip
                    p3 = subprocess.Popen(['sudo', 'python3', '/home/pi/scripts/broadcaster.py'])
                    p3.wait()
                    break
                except IndexError:
                    file.write('\n')
                    file.write('\tssid=')
                    file.write('"')
                    file.write(data[0])
                    file.write('"')

        except:
            print ('ERROR!')
            break

print ("Disconnected, Bluetooth finished.")
server_sock.close()
