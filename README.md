# Wi-Fi Smart Lock

**This project is in progress**

Open a door without having to worry about keys, check the lock status everywhere.
Easy to setup, easy to use.

Requirements:
*  User can setup wifi on the server using the Android application
*  User can setup wifi on the lock using the Android application
*  Android application has a user friendly interface
*  User can do the setup at any given time, and reset settings
*  De lock can still be unlocked using a key in case of power loss

# Hardware


*   `LG G6` to run application
*   `Raspberry Pi Zero W` as server computer
*   `Push Button and LED` as additional hardware

# Software

*   `Android 8.0.0` as operating system for the application
*   `Raspbian` as operation system for Raspberry Pi
*   `Mosquitto` as MQTT Broker
*   `Paho` as MQTT client library

# Done / To Do

**Raspberry Pi**

Done Raspberry Pi:

*  Bluetooth conversion is setup on boot and can be activated whenever
*  Bluetooth conversion begins when push button is pressed for 5 seconds on Raspberry Pi
*  LED blinks during Bluetooth conversion
*  Device is visible to other devices
*  Bluetooth socket waits for connection
*  Bluetooth socket accepts connection from Android devices
*  Bluetooth socket receives strings of text
*  Received data is written to wpa_supplicant.conf file
*  Timeout when no Bluetooth device connects over a certain period of time
*  Callback to Android application when the given password for ssid is not correct
*  Callback to Android application when Wi-Fi connection is established
*  Broadcast the server IP back to Android smartphone or use Bluetooth again (to be decided)

**Android Application**

Done Android Application:

*  Created client using Paho
*  CA certificate check done using SocketFactory
*  TLS connection successfully established to Mosquitto on Raspberry Pi, port 8884
*  Received strings of text from MQTT Broker and displayed in TextField
*  Write / Read to internal text file, to store the server ip
*  Added option to delete text file and enter setup again
*  Added picture of lock when opened and closed, changes on lock state
*  Created Bluetooth client
*  Added option for user to enable Bluetooth when not yet enabled
*  Created Wi-Fi scanning option, displays results in ListView
*  Pressing an element in the list (ssid), creates prompt to input password
*  Ssid of selected element + password are sent to Raspberry Pi over Bluetooth
*  Create feedback to user when process is executing (e.g Bluetooth is connecting)
*  Create socket to receive broadcasted server IP
*  Connection to the received IP
*  Alert message and notification when force is applied to door
*  Notification when door is opened or closed
*  Spinner with currernt locks, and options to add and delete locks
*  Update layout and design of application (XML)

# Bugs

*  Listview shows gaps in the scan results