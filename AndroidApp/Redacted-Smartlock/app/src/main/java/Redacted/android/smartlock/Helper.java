package Redacted.android.smartlock;

import android.bluetooth.BluetoothSocket;
import java.io.OutputStream;

public class Helper {

    private static Helper mHelper;
    private OutputStream output;
    private BluetoothSocket socket;

    private Helper(){

    }

    public static Helper getInstance() {
        if (mHelper != null)
            return mHelper;

        mHelper = new Helper();
        return mHelper;
    }


    public void setOutputStreamer(OutputStream is) {
        output = is;
    }

    public OutputStream getOutputStreamer(){
        return output;
    }

    public void setSocket(BluetoothSocket s) {
        socket = s;
    }

    public BluetoothSocket getSocket() {
        return socket;
    }
}
