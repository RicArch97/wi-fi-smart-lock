package Redacted.android.smartlock;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import timber.log.Timber;

public class Setup extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setup);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        if (readFile(getBaseContext())) {
            Intent enterName = new Intent(getBaseContext(), EnterUserName.class);
            this.startActivity(enterName);
        }
        else {
            //button
            Button start_setup = findViewById(R.id.start_button);

            start_setup.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Intent bluetooth = new Intent(view.getContext(), BluetoothSetup.class);
                    view.getContext().startActivity(bluetooth);
                }
            });
        }
    }

    public Boolean readFile(Context context) {
        BufferedReader in;

        try {
            in = new BufferedReader(new FileReader(new File(context.getFilesDir(), "serveripaddress.txt")));
            if ((in.readLine()) != null) return true;
        } catch (FileNotFoundException e) {
            Timber.v("login activity: file not found %s", e.toString());
        } catch (IOException e) {
            Timber.v("login activity: cannot open file %s", e.toString());
        }

        return false;
    }
}
